/*
 * Устройство получает команды по WIFI и через Arduino Leonardo, с помощью библиотеки HID передает в PC нажатия на мультимедийные клавиши + управление монитором через ИК.
 * Добавлена возможность регулировки громкости через энкодер.
 * 
 * ESP8266 работает в режиме WIFI-клиента, прошит через конструктор https://wifi-iot.com/p/esp8266/, с модулями: "настройки по умолчанию" и "UART через GET"
 *
 *
 * Пример:
 *  Запрос для повышения громкости - http://192.168.1.51/uartpro?speed=9600&send=31 - 31, потому что по ASCII 31hex = 1
 *  для понижения - http://192.168.1.51/uartpro?speed=9600&send=30
 * 
 * 
 * Доступные медиаклавиши можно посмотреть в \libraries\HID-master\src\HID-APIs\ConsumerAPI.h
 * 
 * Включена библиотека IR, для передачи сигнала на включение и выключение монитора.
 * Для китайского клона Leonardo mikro редактируем библиотеку, меняем таймер и пин. В данном случае 5
 */

#include "HID-Project.h"
#include "IRremote.h"       // библиотека для IR, для Leonardo надо откорректировать, как в инструкции и убедиться, что IDE использует нашу библиотеку, а не встроенную
IRsend irsend;              // инициализация IR

char inChar;                // переменная, где храним символ считанный из UART

//====================================
// Блок инициализации для энкодера
unsigned long currentTime;
unsigned long loopTime;
const int pin_A = 6;       // pin 6
const int pin_B = 7;       // pin 7
unsigned char encoder_A;
unsigned char encoder_B;
unsigned char encoder_A_prev=0;
//====================================

void setup() {
  Serial1.begin(9600);      // Serial1, т.к. именно он разведен на пинах Arduino Leonardo, в то время как Serial исключительно для обращения через на USB
  delay(2000);              // желательно оставить задержку для корректной инициализации Arduino Leonardo
  Consumer.begin();         // инициализация медиаклавиш библиотеки "HID-Project.h"
  
  // Определяем пины для подключения энкодера
  pinMode(pin_A, INPUT);
  pinMode(pin_B, INPUT);

  loopTime = currentTime; 
}

void loop() {
  //сначала опрашиваем энкодер
  encoder_A = digitalRead(pin_A);     // считываем состояние выхода А энкодера 
  encoder_B = digitalRead(pin_B);     // считываем состояние выхода B энкодера 

  if((!encoder_A) && (encoder_A_prev)){    // если состояние изменилось с положительного к нулю
  if(encoder_B) {
    // выход В в полож. сост., значит вращение по часовой стрелке
    Consumer.write(MEDIA_VOL_UP);             
  }   
  else {
    // выход В в 0 сост., значит вращение против часовой стрелки     
    Consumer.write(MEDIA_VOL_DOWN);               
  }   
  loopTime = currentTime; 
}   
encoder_A_prev = encoder_A;     // сохраняем значение А для следующего цикла 

  //проверяем наличие данных в UART от модуля ESP8266  
  if (Serial1.available() > 0)  //можно сделать чере функцию Case, но мне так больше нравится
    {
      inChar = Serial1.read(); // записываем символ в переменную

      //Сверяем полученную переменную с нашим списком возможных действий
       if(inChar == '0') 
        {
          Consumer.write(MEDIA_VOL_DOWN); //громкость вниз
          inChar='a'; //пишем в inChar символ, которого нет в нашем списке, что бы в следующем проходе цикла void loop() не выполнить последнее действие повторно
        }
    
       if(inChar == '1') 
        {
          Consumer.write(MEDIA_VOL_UP); //громкость вверх
          inChar='a';
        }
        
       if(inChar == '2') 
        {
          Consumer.write(MEDIA_NEXT); // следующий трек //Может потребоватся изменение на MEDIA_REWIND или другой аналог из перечня доступных вариаций медикнопок из ConsumerAPI.h, в зависимости от ОС
          inChar='a';
        }

       if(inChar == '3') 
        {
          Consumer.write(MEDIA_PREV); //предидущий трек
          inChar='a';
        }

       if(inChar == '4') 
        {
          Consumer.write(MEDIA_STOP); //Стоп
          inChar='a';
        }

       if(inChar == '5') 
        {
          Consumer.write(MEDIA_PLAY_PAUSE); //Play/Pause
          inChar='a';
        }
        
       if(inChar == '6') 
        {
         irsend.sendNEC(0x60c40bf, 32);   //отправка сигнала Power ON/OFF через ИК-светодиод. Код посылки непосредственно под мой монитор
         inChar='a';
        }
       
       if(inChar == '7') 
        {
          Consumer.write(MEDIA_VOL_MUTE); //Mute
          inChar='a';
        }
    }
}
